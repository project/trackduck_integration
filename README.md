INTRODUCTION
TrackDuck - Visual Feedback & Bug Tracking

Let your colleagues or clients leave feedback right on a website or image file. 
Bug tracking has never been easier! Every new comment has a screenshot recorded 
automatically with a link to the exact page and technical details like browser 
version, OS and screen resolution.

REQUIREMENTS
*trackduck account

INSTALLATION
composer require drupal/trackduck_integration

CONFIGURATION
*Create trackduck account and setup your project
*Go to: admin/config/development/trackduck
*Setup your api key
*Check permissions for correct role
